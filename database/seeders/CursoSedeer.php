<?php

namespace Database\Seeders;

use App\Models\Curso;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CursoSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $curso = new Curso();

        $curso->nombre = "Laravel";
        $curso->descripcion = "El mejor Framework de PHP";
        $curso->categoria = " Desarrollo web";

        $curso->save();

        $curso2 = new Curso();

        $curso2->nombre = "Laravel";
        $curso2->descripcion = "El mejor Framework de PHP";
        $curso2->categoria = " Desarrollo web";

        $curso2->save();
        //
    }
}
