@extends('layouts.auth-master')

@section('content')

<form action="/register" method="post">
    @csrf
    
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      
     
    <div class="form-group mb-lg">
        <label>Nombre   </label>
        <div class="input-group input-group-icon">
            <input name="name" type="text" value="{{ old('name') }}" class="form-control input-lg" />
            @if ($errors->has('name'))
            <span class="text-danger text-left">{{ $errors->first('name') }}</span>
            @endif
            <span class="input-group-addon">
                <span class="icon icon-lg">
                    <i class="fa fa-user"></i>
                </span>
            </span>
        </div>
        
    </div>

    <div class="form-group mb-lg">
        <label>Nombre de Usuario   </label>
        <div class="input-group input-group-icon">
            <input name="username" type="text" value="{{ old('username') }}" class="form-control input-lg" />
            @if ($errors->has('username'))
            <span class="text-danger text-left">{{ $errors->first('username') }}</span>
            @endif
            <span class="input-group-addon">
                <span class="icon icon-lg">
                    <i class="fa fa-user"></i>
                </span>
            </span>
        </div>
        
    </div>

    <div class="form-group mb-lg">
        <label>Email</label>
        <div class="input-group input-group-icon">
            <input name="email" type="text" value="{{ old('email') }}" class="form-control input-lg" />
            @if ($errors->has('email'))
            <span class="text-danger text-left">{{ $errors->first('email') }}</span>
            @endif
            <span class="input-group-addon">
                <span class="icon icon-lg">
                    <i class="fa fa-user"></i>
                </span>
            </span>
        </div>
        
    </div>
    <div class="form-group mb-lg">
        <div class="clearfix">
            <label class="pull-left">Password</label>
            
        </div>
        <div class="input-group input-group-icon">
            <input name="password" type="password" value="{{ old('password') }}" class="form-control input-lg" />
            @if ($errors->has('password'))
                <span class="text-danger text-left">  {{ $errors->first('password') }}</span>
            @endif
            <span class="input-group-addon">
                <span class="icon icon-lg">
                    <i class="fa fa-lock"></i>
                </span>
            </span>
        </div>
      
    </div>
    <div class="form-group mb-lg">
        <div class="clearfix">
            <label class="pull-left">Password</label>
            
        </div>
        <div class="input-group input-group-icon">
            <input name="password_confirmation" type="password" value="{{ old('password_confirmation') }}" class="form-control input-lg" />
            @if ($errors->has('password'))
                <span class="text-danger text-left">  {{ $errors->first('password_confirmation') }}</span>
            @endif
            <span class="input-group-addon">
                <span class="icon icon-lg">
                    <i class="fa fa-lock"></i>
                </span>
            </span>
        </div>
      
    </div>
    
    <div class="row">
        <div class="col-sm-8">
            <div class="checkbox-custom checkbox-default">
                <input id="RememberMe" name="rememberme" type="checkbox"/>
                <label for="RememberMe">Remember Me</label>
            </div>
        </div>
        <div class="col-sm-4 text-right">
            <button type="submit" class="btn btn-primary hidden-xs">Sign In</button>
            <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
        </div>
    </div>

    <span class="mt-lg mb-lg line-thru text-center text-uppercase">
        <span>or</span>
    </span>

    <div class="mb-xs text-center">
        <a class="btn btn-facebook mb-md ml-xs mr-xs">Connect with <i class="fa fa-facebook"></i></a>
        <a class="btn btn-twitter mb-md ml-xs mr-xs">Connect with <i class="fa fa-twitter"></i></a>
    </div>

    <p class="text-center">Don't have an account yet? <a href="/register">Sign Up!</a></p>
    @include('auth.partials.copy')
</form>
@endsection