@extends('layouts.auth-master')

@section('content')



    <form action="{{ route('login.perform') }}" method="post">
        @csrf
        <div class="form-group mb-lg">
            <label>Email ó Nombre de Usuario   </label>
            <div class="input-group input-group-icon">
                <input name="username" type="text" value="{{ old('username') }}" class="form-control input-lg" />
                @if ($errors->has('username'))
                <span class="text-danger text-left">{{ $errors->first('username') }}</span>
                @endif
                <span class="input-group-addon">
                    <span class="icon icon-lg">
                        <i class="fa fa-user"></i>
                    </span>
                </span>
            </div>
        </div>

        <div class="form-group mb-lg">
            <div class="clearfix">
                <label class="pull-left">Password</label>
                <a href="pages-recover-password.html" class="pull-right">Lost Password?</a>
            </div>
            <div class="input-group input-group-icon">
                <input name="password" type="password" value="{{ old('password') }}" class="form-control input-lg" />
                @if ($errors->has('password'))
                    <span class="text-danger text-left">  {{ $errors->first('password') }}</span>
                @endif
                <span class="input-group-addon">
                    <span class="icon icon-lg">
                        <i class="fa fa-lock"></i>
                    </span>
                </span>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8">
                <div class="checkbox-custom checkbox-default">
                    <input id="RememberMe" name="rememberme" type="checkbox"/>
                    <label for="RememberMe">Recuerdarme</label>
                </div>
            </div>
            <div class="col-sm-4 text-right">
                <button type="submit" class="btn btn-primary hidden-xs">Iniciar Sesión</button>
                <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
            </div>
        </div>

        <span class="mt-lg mb-lg line-thru text-center text-uppercase">
            <span>Ó</span>
        </span>


        <p class="text-center">¿Aún no tienes una cuenta?<a href="/register"> ¡Inscribirse!</a></p>
        @include('auth.partials.copy')
    </form>
@endsection