<!doctype html>
<html class="fixed has-top-menu">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Default Layout | Porto Admin - Responsive HTML5 Template 1.7.0</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{!! url('assets/vendor/bootstrap/css/bootstrap.css') !!}" />

		<link rel="stylesheet" href="{!! url('assets/vendor/font-awesome/css/font-awesome.css') !!}" />
		<link rel="stylesheet" href="{!! url('assets/vendor/magnific-popup/magnific-popup.css') !!}" />
		<link rel="stylesheet" href="{!! url('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css') !!}" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="{!! url('assets/vendor/jquery-ui/jquery-ui.css') !!}" />
		<link rel="stylesheet" href="{!! url('assets/vendor/jquery-ui/jquery-ui.theme.css') !!}" />
		<link rel="stylesheet" href="{!! url('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') !!}" />
		<link rel="stylesheet" href="{!! url('assets/vendor/morris.js/morris.css') !!}" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{!! url('assets/stylesheets/theme.css') !!}" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="{!! url('assets/stylesheets/skins/default.css') !!}" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{!! url('assets/stylesheets/theme-custom.css') !!}">

		<!-- Head Libs -->
		<script src="{!! url('assets/vendor/modernizr/modernizr.js') !!}"></script>

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header header-nav-menu">
				<div class="logo-container">
					<a href="../" class="logo">
						<img src="{!! url('assets/images/logo.png') !!}" width="75" height="35" alt="Porto Admin" />
					</a>
					<button class="btn header-btn-collapse-nav hidden-md hidden-lg" data-toggle="collapse" data-target=".header-nav">
						<i class="fa fa-bars"></i>
					</button>
			
					<!-- start: header nav menu -->
					<div class="header-nav collapse">
						<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
							<nav>
								<ul class="nav nav-pills" id="mainNav">
									<li class="">
									    <a href="layouts-default.html">
									        Dashboard
									    </a>    
									</li>
									<li class="dropdown active">
									    <a class="dropdown-toggle" href="#">
									        Layouts
									    </a>
									    <ul class="dropdown-menu">
									        <li>
									            <a href="index.html">
									                Landing Page
									            </a>
									        </li>
									        <li>
									            <a href="layouts-default.html">
									                Default
									            </a>
									        </li>
									        <li class="dropdown-submenu">
									            <a>
									                Boxed
									            </a>
									            <ul class="dropdown-menu">
									                <li>
									                    <a href="layouts-boxed.html">
									                        Static Header
									                    </a>
									                </li>
									                <li>
									                    <a href="layouts-boxed-fixed-header.html">
									                        Fixed Header
									                    </a>
									                </li>
									            </ul>
									        </li>
									        <li class="dropdown-submenu">
									            <a>
									                Horizontal Menu Header
									            </a>
									            <ul class="dropdown-menu">
									                <li>
									                    <a href="layouts-header-menu.html">
									                        Pills
									                    </a>
									                </li>
									                <li>
									                    <a href="layouts-header-menu-stripe.html">
									                        Stripe
									                    </a>
									                </li>
									                <li>
									                    <a href="layouts-header-menu-top-line.html">
									                        Top Line
									                    </a>
									                </li>
									            </ul>
									        </li>
									        <li>
									            <a href="layouts-dark.html">
									                Dark
									            </a>
									        </li>
									        <li>
									            <a href="layouts-dark=header.html">
									                Dark Header
									            </a>
									        </li>
									        <li>
									            <a href="layouts-two-navigations.html">
									                Two Navigations
									            </a>
									        </li>
									        <li class="dropdown-submenu">
									            <a>
									                Tab Navigation
									            </a>
									            <ul class="dropdown-menu">
									                <li>
									                    <a href="layouts-tab-navigation-dark.html">
									                        Tab Navigation Dark
									                    </a>
									                </li>
									                <li>
									                    <a href="layouts-tab-navigation.html">
									                        Tab Navigation Light
									                    </a>
									                </li>
									                <li>
									                    <a href="layouts-tab-navigation-boxed.html">
									                        Tab Navigation Boxed
									                    </a>
									                </li>
									            </ul>
									        </li>
									        <li>
									            <a href="layouts-light-sidebar.html">
									                Light Sidebar
									            </a>
									        </li>
									        <li>
									            <a href="layouts-left-sidebar-collapsed.html">
									                Left Sidebar Collapsed
									            </a>
									        </li>
									        <li>
									            <a href="layouts-left-sidebar-scroll.html">
									                Left Sidebar Scroll
									            </a>
									        </li>
									        <li class="dropdown-submenu">
									            <a>
									                Left Sidebar Big Icons
									            </a>
									            <ul class="dropdown-menu">
									                <li>
									                    <a href="layouts-left-sidebar-big-icons.html">
									                        Left Sidebar Big Icons Dark
									                    </a>
									                </li>
									                <li>
									                    <a href="layouts-left-sidebar-big-icons-light.html">
									                        Left Sidebar Big Icons Light
									                    </a>
									                </li>
									            </ul>
									        </li>
									        <li class="dropdown-submenu">
									            <a>
									                Left Sidebar Panel
									            </a>
									            <ul class="dropdown-menu">
									                <li>
									                    <a href="layouts-left-sidebar-panel.html">
									                        Left Sidebar Panel Dark
									                    </a>
									                </li>
									                <li>
									                    <a href="layouts-left-sidebar-panel-light.html">
									                        Left Sidebar Panel Light
									                    </a>
									                </li>
									            </ul>
									        </li>
									        <li class="dropdown-submenu">
									            <a>
									                Left Sidebar Sizes
									            </a>
									            <ul class="dropdown-menu">
									                <li>
									                    <a href="layouts-sidebar-sizes-xs.html">
									                        Left Sidebar XS
									                    </a>
									                </li>
									                <li>
									                    <a href="layouts-sidebar-sizes-sm.html">
									                        Left Sidebar SM
									                    </a>
									                </li>
									                <li>
									                    <a href="layouts-sidebar-sizes-md.html">
									                        Left Sidebar MD
									                    </a>
									                </li>
									            </ul>
									        </li>
									        <li>
									            <a href="layouts-square-borders.html">
									                Square Borders
									            </a>
									        </li>
									    </ul>
									</li>
									<li class="dropdown">
									    <a class="dropdown-toggle" href="#">
									        Pages
									    </a>
									    <ul class="dropdown-menu">
									        <li>
									            <a href="pages-signup.html">
									                Sign Up
									            </a>
									        </li>
									        <li>
									            <a href="pages-signin.html">
									                Sign In
									            </a>
									        </li>
									        <li>
									            <a href="pages-reover-password.html">
									                Recover Password
									            </a>
									        </li>
									        <li>
									            <a href="pages-lock-screen.html">
									                Locked Screen
									            </a>
									        </li>
									        <li>
									            <a href="pages-user-profile.html">
									                User Profile
									            </a>
									        </li>
									        <li>
									            <a href="pages-session-timeout.html">
									                Session Timeout
									            </a>
									        </li>
									        <li>
									            <a href="pages-calendar.html">
									                Calendar
									            </a>
									        </li>
									        <li>
									            <a href="pages-timeline.html">
									                Timeline
									            </a>
									        </li>
									        <li>
									            <a href="pages-media-gallery.html">
									                Media Gallery
									            </a>
									        </li>
									        <li>
									            <a href="pages-invoice.html">
									                Invoice
									            </a>
									        </li>
									        <li>
									            <a href="pages-blank.html">
									                Blank Page
									            </a>
									        </li>
									        <li>
									            <a href="pages-404.html">
									                404
									            </a>
									        </li>
									        <li>
									            <a href="pages-500.html">
									                500
									            </a>
									        </li>
									        <li>
									            <a href="pages-log-viewer.html">
									                Log Viewer
									            </a>
									        </li>
									        <li>
									            <a href="pages-search-results.html">
									                Search Results
									            </a>
									        </li>
									    </ul>
									</li>
									<li class="dropdown dropdown-mega">
									    <a class="dropdown-toggle" href="#">UI Elements</a>
									    <ul class="dropdown-menu">
									        <li>
									            <div class="dropdown-mega-content">
									                <div class="row">
									                    <div class="col-md-3">
									                        <ul class="dropdown-mega-sub-nav">
									                            <li>
									                                <a href="ui-elements-typography.html">
									                                    Typography
									                                </a>
									                            </li>
									                            <li>
									                                <a href="#">
									                                    Icons <span class="mega-sub-nav-toggle toggled pull-right" data-toggle="collapse" data-target=".mega-sub-nav-sub-menu-1"></span>
									                                </a>
									                                <ul class="dropdown-mega-sub-nav mega-sub-nav-sub-menu-1 collapse in">
									                                    <li>
									                                        <a href="ui-elements-icons-elusive.html">
									                                            Elusive
									                                        </a>
									                                    </li>
									                                    <li>
									                                        <a href="ui-elements-icons-font-awesome.html">
									                                            Font Awesome
									                                        </a>
									                                    </li>
									                                    <li>
									                                        <a href="ui-elements-icons-glyphicons.html">
									                                            Glyphicons
									                                        </a>
									                                    </li>
									                                    <li>
									                                        <a href="ui-elements-icons-line-icons.html">
									                                            Line Icons
									                                        </a>
									                                    </li>
									                                    <li>
									                                        <a href="ui-elements-icons-meteocons.html">
									                                            Meteocons
									                                        </a>
									                                    </li>
									                                </ul>
									                            </li>
									                            <li>
									                                <a href="ui-elements-tabs.html">
									                                    Tabs
									                                </a>
									                            </li>
									                        </ul>
									                    </div>
									                    <div class="col-md-3">
									                        <ul class="dropdown-mega-sub-nav">
									                            <li>
									                                <a href="ui-elements-panels.html">
									                                    Panels
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-widgets.html">
									                                    Widgets
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-portlets.html">
									                                    Portlets
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-buttons.html">
									                                    Buttons
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-alerts.html">
									                                    Alerts
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-notifications.html">
									                                    Notifications
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-modals.html">
									                                    Modals
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-lightbox.html">
									                                    Lightbox
									                                </a>
									                            </li>
									                        </ul>
									                    </div>
									                    <div class="col-md-3">
									                        <ul class="dropdown-mega-sub-nav">
									                            <li>
									                                <a href="ui-elements-progressbars.html">
									                                    Progress Bars
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-sliders.html">
									                                    Sliders
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-carousels.html">
									                                    Carousels
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-accordions.html">
									                                    Accordions
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-nestable.html">
									                                    Nestable
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-tree-view.html">
									                                    Tree View
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-scrollable.html">
									                                    Scrollable
									                                </a>
									                            </li>
									                            <li>
									                                <a href="ui-elements-grid-system.html">
									                                    Grid System
									                                </a>
									                            </li>
									                        </ul>
									                    </div>
									                    <div class="col-md-3">
									                        <ul class="dropdown-mega-sub-nav">
									                            <li>
									                                <a href="ui-elements-charts.html">
									                                    Charts
									                                </a>
									                            </li>
									                            <li>
									                                <a href="#">
									                                    Animations <span class="mega-sub-nav-toggle pull-right" data-toggle="collapse" data-target=".mega-sub-nav-sub-menu-2"></span>
									                                </a>
									                                <ul class="dropdown-mega-sub-nav mega-sub-nav-sub-menu-2 collapse">
									                                    <li>
									                                        <a href="ui-elements-animations-appear.html">
									                                            Appear
									                                        </a>
									                                    </li>
									                                    <li>
									                                        <a href="ui-elements-animations-hover.html">
									                                            Hover
									                                        </a>
									                                    </li>
									                                </ul>
									                            </li>
									                            <li>
									                                <a href="#">
									                                    Loading <span class="mega-sub-nav-toggle pull-right" data-toggle="collapse" data-target=".mega-sub-nav-sub-menu-3"></span>
									                                </a>
									                                <ul class="dropdown-mega-sub-nav mega-sub-nav-sub-menu-3 collapse">
									                                    <li>
									                                        <a href="ui-elements-loading-overlay.html">
									                                            Overlay
									                                        </a>
									                                    </li>
									                                    <li>
									                                        <a href="ui-elements-loading-progress.html">
									                                            Progress
									                                        </a>
									                                    </li>
									                                </ul>
									                            </li>
									                            <li>
									                                <a href="ui-elements-extra.html">
									                                    Extra
									                                </a>
									                            </li>
									                        </ul>
									                    </div>
									                </div>
									            </div>
									        </li>
									    </ul>
									</li>
									<li class="dropdown">
									    <a href="#" class="dropdown-toggle">More</a>
									    <ul class="dropdown-menu">
									        <li class="dropdown-submenu">
									            <a href="#">
									                Forms
									            </a>
									            <ul class="dropdown-menu">
									                <li>
									                    <a href="forms-basic.html">
									                        Basic
									                    </a>
									                </li>
									                <li>
									                    <a href="forms-advanced.html">
									                        Advanced
									                    </a>
									                </li>
									                <li>
									                    <a href="forms-validation.html">
									                        Validation
									                    </a>
									                </li>
									                <li>
									                    <a href="forms-layouts.html">
									                        Layouts
									                    </a>
									                </li>
									                <li>
									                    <a href="forms-wizard.html">
									                        Wizard
									                    </a>
									                </li>
									                <li>
									                    <a href="forms-code-editor.html">
									                        Code Editor
									                    </a>
									                </li>
									            </ul>
									        </li>
									        <li class="dropdown-submenu">
									            <a href="#">
									                Tables
									            </a>
									            <ul class="dropdown-menu">
									                <li>
									                    <a href="tables-basic.html">
									                        Basic
									                    </a>
									                </li>
									                <li>
									                    <a href="tables-advanced.html">
									                        Advanced
									                    </a>
									                </li>
									                <li>
									                    <a href="tables-responsive.html">
									                        Responsive
									                    </a>
									                </li>
									                <li>
									                    <a href="tables-editable.html">
									                        Editable
									                    </a>
									                </li>
									                <li>
									                    <a href="tables-ajax.html">
									                        Ajax
									                    </a>
									                </li>
									                <li>
									                    <a href="tables-pricing.html">
									                        Pricing
									                    </a>
									                </li>
									            </ul>
									        </li>
									        <li>
									            <a href="mailbox-folder.html">
									                Mailbox
									                <span class="pull-right label label-primary">182</span>
									            </a>            
									        </li>
									        <li class="dropdown-submenu">
									            <a href="#">
									                Maps
									            </a>
									            <ul class="dropdown-menu">
									                <li>
									                    <a href="maps-google-maps.html">
									                        Basic
									                    </a>
									                </li>
									                <li>
									                    <a href="maps-google-maps-builder.html">
									                        Map Builder
									                    </a>
									                </li>
									                <li>
									                    <a href="maps-vector.html">
									                        Vector
									                    </a>
									                </li>
									            </ul>
									        </li>
									        <li class="dropdown-submenu">
									            <a href="#">
									                Extra
									            </a>
									            <ul class="dropdown-menu">
									                <li>
									                    <a href="extra-changelog.html">
									                        Changelog
									                    </a>
									                </li>
									                <li>
									                    <a href="extra-ajax-made-easy.html">
									                        Ajax Made Easy
									                    </a>
									                </li>
									            </ul>
									        </li>
									        <li>
									            <a href="http://themeforest.net/item/porto-responsive-html5-template/4106987?ref=Okler">
									                Front-End <span class="tip tip-dark">hot</span><em class="not-included">(Not Included)</em>
									            </a>            
									        </li>
									        <li class="dropdown-submenu">
									            <a href="#">
									                Menu Levels
									            </a>
									            <ul class="dropdown-menu">
									                <li>
									                    <a>
									                        First Level
									                    </a>
									                </li>
									                <li class="dropdown-submenu">
									                    <a href="#">
									                        Second Level
									                    </a>
									                    <ul class="dropdown-menu">
									                        <li>
									                            <a>
									                                Second Level Link #1
									                            </a>
									                        </li>
									                        <li>
									                            <a>
									                                Second Level Link #2
									                            </a>
									                        </li>
									                        <li class="dropdown-submenu">
									                            <a href="#">
									                                Third Level
									                            </a>
									                            <ul class="dropdown-menu">
									                                <li>
									                                    <a>
									                                        Third Level Link #1
									                                    </a>
									                                </li>
									                                <li>
									                                    <a>
									                                        Third Level Link #2
									                                    </a>
									                                </li>
									                            </ul>
									                        </li>
									                    </ul>
									                </li>
									            </ul>
									        </li>
									    </ul>
									</li>
									
								</ul>
							</nav>
						</div>
					</div>
          					<!-- end: header nav menu -->
				</div>
        	<!-- start: search & user box -->
				<div class="header-right">
			
				
					
					
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="assets/images/!logged-user.jpg" alt=" {{auth()->user()->name}}" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name=" {{auth()->user()->name}}" data-lock-email="johndoe@okler.com">
								<span class="name"> {{auth()->user()->name}}</span>
								<span class="role">administrator</span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="/logout"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->  
    </div>
  </div>
</header>