<!doctype html>
<html class="fixed">
	<head>
		

		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <title>Laravel</title>
		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{!! url('assets/vendor/bootstrap/css/bootstrap.css') !!}" />

		<link rel="stylesheet" href="{!! url('assets/vendor/font-awesome/css/font-awesome.css') !!}" />
		<link rel="stylesheet" href="{!! url('assets/vendor/magnific-popup/magnific-popup.css') !!}" />
		<link rel="stylesheet" href="{!! url('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css') !!}" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{!! url('assets/stylesheets/theme.css') !!}" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="{!! url('assets/stylesheets/skins/default.css') !!}" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{!! url('assets/stylesheets/theme-custom.css') !!}">

		<!-- Head Libs -->
		<script src="{!! url('assets/vendor/modernizr/modernizr.js') !!}"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo pull-left">
					<img src="{!! url('assets/images/logo.png') !!}" height="54" alt="Porto Admin" />
				</a>

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i>Iniciar Sesión</h2>
					</div>
					<div class="panel-body">
            @yield('content')
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md">&copy; Copyright {{date('Y')}}. All Rights Reserved.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="{!! url('assets/vendor/jquery/jquery.js') !!}"></script>
		<script src="{!! url('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') !!}"></script>
		<script src="{!! url('assets/vendor/bootstrap/js/bootstrap.js') !!}"></script>
		<script src="{!! url('assets/vendor/nanoscroller/nanoscroller.js') !!}"></script>
		<script src="{!! url('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
		<script src="{!! url('assets/vendor/magnific-popup/jquery.magnific-popup.js') !!}"></script>
		<script src="{!! url('assets/vendor/jquery-placeholder/jquery-placeholder.js') !!}"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="{!! url('assets/javascripts/theme.js') !!}"></script>
		
		<!-- Theme Custom -->
		<script src="{!! url('assets/javascripts/theme.custom.js') !!}"></script>
		
		<!-- Theme Initialization Files -->
		<script src="{!! url('assets/javascripts/theme.init.js') !!}"></script>

	</body>
</html>

